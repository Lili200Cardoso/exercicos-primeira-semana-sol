﻿using Exercicios;


var linha = InterfaceTela.ImprimirTraco(100, "-");
//Exercício 1
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 1");

var exec01_Raiz = new Exec01_RaizAproximada();
var raizQuadradaAproximada = exec01_Raiz.CalculaRaizQuadradaAproximada(10);

Console.WriteLine(raizQuadradaAproximada);

var raizCubicaAproximada = exec01_Raiz.CalculaRaizCubicaAproximada(10);

Console.WriteLine(raizCubicaAproximada);
Console.WriteLine($"{linha}");

//Exercício 2
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 2");

var exec02_PI = new Exec02_PI();
var piAproximado = exec02_PI.SomaAproximadaPI();

Console.WriteLine(piAproximado);



Console.WriteLine($"{linha}");





//Exercício 3
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 3");

var exec03_Randomizador = new Exec03_Randomizador();
var numerosRadomicos = exec03_Randomizador.Randomizar();
var numerosRandomicosPares = exec03_Randomizador.NumerosRadomicosPares(numerosRadomicos);
var numerosRandomicosImpares = exec03_Randomizador.NumerosRandomicosImpares(numerosRadomicos);
var somaTotalNumerosRandomicos = exec03_Randomizador.SomaTodosNumerosRandomicos(numerosRadomicos);
var menorNumerosRandomicos = exec03_Randomizador.NumerosRandomicosMenor(numerosRadomicos);
var maiorNumerosRandomicos = exec03_Randomizador.NumerosRandomicosMaior(numerosRadomicos);

Console.WriteLine("Números Aleatórios Gerados: ");
for(int i = 0; i < numerosRadomicos.Count(); i++)
{
    Console.Write($"{numerosRadomicos[i]}, ");
    
}
Console.WriteLine("\n\n");
Console.WriteLine($"Quantidade de numeros radomicos pares: {numerosRandomicosPares}");
Console.WriteLine($"Quantidade de números randomicos ímpares: {numerosRandomicosImpares}");
Console.WriteLine($"A soma de todos os números gerados: {somaTotalNumerosRandomicos}");
Console.WriteLine($"O menor valor dentre os números gerados: {menorNumerosRandomicos}");
Console.WriteLine($"O maior valor dentre os números gerados: {maiorNumerosRandomicos}");
Console.WriteLine($"{linha}");


//Exercicio 4
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 4");

var exec04_ConversorDeTemperatura = new Exec04_ConversorDeTemperatura();
var temperaturasFahreinheitConvertida = exec04_ConversorDeTemperatura.ConverterFahreinheitParaCelsius();

foreach (var temperaturaCelsius in temperaturasFahreinheitConvertida)
{
    Console.WriteLine(temperaturaCelsius);
}
Console.WriteLine($"{linha}");


//Exercicio 5
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 5");

var exec05_NumerosPrimos = new Exec05_NumerosPrimos();

Console.WriteLine("Todos os números primos até 20.000");
for(int i = 0; i <= 20000; i++)
{
    var resultado = exec05_NumerosPrimos.EhPrimo(i);
    if(resultado == true)
    {
        Console.Write($"{i} ,");
    }

}
Console.WriteLine("\n\n");
Console.WriteLine($"{linha}");


//Exercicio 6
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 6");

var exec06_NumerosAleatoriosSemRepeticao = new Exec06_NumerosAleatoriosSemRepeticao();
var numerosAleatorios = exec06_NumerosAleatoriosSemRepeticao.ObterNumerosAleatorios();

Console.WriteLine("Números Aleatórios NÃO REPETIDOS Gerados: ");

for (int i = 0; i < numerosAleatorios.Count(); i++)
{
    Console.Write($"{numerosAleatorios[i]}, ");

}
Console.WriteLine("\n\n");
Console.WriteLine($"{linha}");

//Exercicio 7
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 7");
Console.WriteLine("Ordenando os Números Aleatórios NÃO REPETIDOS: ");

numerosAleatorios.Sort();

foreach (int x in  numerosAleatorios)
{
    Console.Write($"{x}, ");

}
Console.WriteLine("\n\n");
Console.WriteLine($"{linha}");

//Exercicio 8
Console.WriteLine($"{linha}");
Console.WriteLine("EXERCICIO 8");

var exec08_UniaoIntersecao = new Exec08_UniaoIntersecao();
var conjunto_1 = exec08_UniaoIntersecao.GerarLista();
var conjunto_2 = exec08_UniaoIntersecao.GerarLista();
var conjunto_Uniao = exec08_UniaoIntersecao.Uniao(conjunto_1, conjunto_2);
var conjunto_Intersecao = exec08_UniaoIntersecao.Intersecao(conjunto_1, conjunto_2);

Console.WriteLine("Primeiro Conjunto: ");
foreach(int item in conjunto_1)
{
    Console.Write($"{item} ");

}
Console.WriteLine("\n\n\n");
Console.WriteLine("Segundo Conjunto: ");
foreach (int item in conjunto_2)
{
    Console.Write($"{item} ");
}
Console.WriteLine("\n\n\n");
Console.WriteLine("Mostrando conjunto União: ");
foreach(int item in conjunto_Uniao)
{
    Console.Write($"{item} ");
}
Console.WriteLine("\n\n\n");
Console.WriteLine("Mostrando conjunto Interceção: ");
foreach(int item in conjunto_Intersecao)
{
    Console.Write($"{item} ");
}
Console.WriteLine("\n\n\n");
Console.WriteLine($"{linha}");
Console.ReadKey();




