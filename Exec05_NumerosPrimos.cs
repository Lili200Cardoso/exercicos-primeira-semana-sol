﻿namespace Exercicios
{
    public class Exec05_NumerosPrimos
    {
        public bool EhPrimo(int numero)
        {
            
            int divisores = 0; 
            for(int i=1; i<=numero; i++)
            {
                if (numero % i == 0)
                    divisores++;
            }
            if(divisores == 2)
                return true;
            else
                return false;
        }
    }
}
