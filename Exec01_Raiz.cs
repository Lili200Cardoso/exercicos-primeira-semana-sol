﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicios
{
    public  class Exec01_RaizAproximada
    {
        public double CalculaRaizQuadradaAproximada(double numeroReal)
        {
            double erro = double.MaxValue;
            double resultado = 0;            
            double xi = 1;

            while(erro > 0.0001)
            {

                resultado =(double) 1/2 * (xi + numeroReal / 2);
                erro = resultado - xi;

                if(erro > 0.0001)
                {
                    xi = resultado;
                }

            }
            return resultado;
        }

        public double CalculaRaizCubicaAproximada(double numeroReal)
        {
            double erro = double.MaxValue;
            double resultado = 0;
            double xi = 1;

            while (erro > 0.0001)
            {                
                resultado = (double)(2 * Math.Pow(xi,3) + numeroReal)/(3 *Math.Pow(xi,2));
                erro = resultado - xi;

                if (erro > 0.0001)
                {
                    xi = resultado;
                }

            }
            return resultado;
        }



    }
}
