﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicios
{
    public class Exec08_UniaoIntersecao
    {
        public List<int> GerarLista()
        {
            Random rand = new Random();
            List<int> lista = new List<int>();
            for(int i = 0; i < 20; i++)
            {
                int numero = rand.Next(100, 200);
                lista.Add(numero);
            }
            return lista;
        }

        public List<int> Uniao(List<int> Conjunto_1, List<int> Conjunto_2)
        {
            List<int> Conjunto_Uniao = new List<int>();
            foreach(int item in Conjunto_1)
            {
                Conjunto_Uniao.Add(item);
            }
            foreach(int item in Conjunto_2)
            {
                Conjunto_Uniao.Add(item);
            }
            return Conjunto_Uniao;

        }

        public List<int> Intersecao(List<int> Conjunto_1, List<int> Conjunto_2)
        {
            List<int> Conjunto_intersecao = new List<int>();
            List<int> Conjunto_Uniao = Uniao(Conjunto_1, Conjunto_2);

            foreach(int item in Conjunto_Uniao)
            {
                var existeConjunto_1 = Conjunto_1.Exists(e => e == item);
                var existeConjunto_2 = Conjunto_2.Exists(e => e == item);


                if(existeConjunto_1 && existeConjunto_2)
                {
                    Conjunto_intersecao.Add(item);
                }
            }
            return Conjunto_intersecao;
        }

    }
}
