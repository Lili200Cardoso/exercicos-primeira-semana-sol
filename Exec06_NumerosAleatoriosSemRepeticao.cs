﻿namespace Exercicios
{
    public class Exec06_NumerosAleatoriosSemRepeticao
    {
       

        public List<int> ObterNumerosAleatorios()
        {
            List<int> numeros = new List<int>();
            Random rand = new Random();
          
            for (int i = 0; i <= 100; i++)
            {
                var numero = rand.Next(-500, 501);
                if (!numeros.Any(e => e == numero))
                {
                    numeros.Add(numero);
                }
                i = numeros.Count;
            }

            numeros.Sort();
            return numeros;
        }
    }
}