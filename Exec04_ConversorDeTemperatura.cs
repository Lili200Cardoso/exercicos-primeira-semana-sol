﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicios
{
    public class Exec04_ConversorDeTemperatura
    {
   
        public List<double> ConverterFahreinheitParaCelsius()
        {
            List<double> lista = new List<double>();
            double celsius = 0;
  
            for(int fahreinheit = 0; fahreinheit <= 200; fahreinheit += 10)
            {
                celsius = Math.Round((double) 5/9 * (fahreinheit - 32),2);                
                lista.Add(celsius);
            }
 
            return lista ;
        }


    }
}
