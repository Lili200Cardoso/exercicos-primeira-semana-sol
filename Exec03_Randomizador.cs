 namespace Exercicios;

public class Exec03_Randomizador
{    
    public  int[] Randomizar()
    {
        Random rand = new Random();
        int[] numeros = new int[200];

        for (int i = 0; i < numeros.Length; i++)
        {
            numeros[i] = rand.Next(10000);
        }

        return numeros;
    }

    public int NumerosRadomicosPares(int[] numerosRadomicos)
    {
        int numerosPares = 0;
        for (int i = 0; i < numerosRadomicos.Length; i++)
        {
            if (numerosRadomicos[i] % 2 == 0)
            {
                numerosPares++;
            }
        }

        return numerosPares;
    }

    public int NumerosRandomicosImpares(int[] numerosRandomicos)
    {
        int numerosImpares = 0;
        for(int i = 0; i < numerosRandomicos.Length; i++)
        {
            if (numerosRandomicos[i] % 2 != 0)
            {
                numerosImpares++;
            }
        }
        return numerosImpares;
    }

    public int SomaTodosNumerosRandomicos(int[] numerosRandomicos)
    {
        int totalDaSoma = numerosRandomicos.Sum();
        return totalDaSoma;
    }

    public int NumerosRandomicosMenor(int[] numerosRandomicos)
    {
        int menor = numerosRandomicos[0];
        for(int i = 1; i < numerosRandomicos.Length; i++)
        {
            if(numerosRandomicos[i] < menor)
            {
                menor = numerosRandomicos[i];
                
            } 
            
        }
        return menor;
    }

        public int NumerosRandomicosMaior(int[] numerosRandomicos)
    {
        int maior = 0;
        for(int i = 1; i < numerosRandomicos.Length; i++)
        {
            if(numerosRandomicos[i] >
             maior)
            {
                maior = numerosRandomicos[i];
                
            } 
            
        }
        return maior;
    }
}